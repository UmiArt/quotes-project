import React from "react";
import {BrowserRouter, Switch, Route, NavLink} from "react-router-dom";
import Quotes from "./Conteiner/Quotes/Quotes";
import SubmitQuote from "./Conteiner/SubmitQuote/SubmitQuote";
import Star from "./Conteiner/Quotes/Componrnt/Star/Star";
import People from "./Conteiner/Quotes/Componrnt/People/People";
import Humor from "./Conteiner/Quotes/Componrnt/Humor/Humor";
import Edit from "./Conteiner/Quotes/Componrnt/Edit/Edit";
import Saying from "./Conteiner/Quotes/Componrnt/Saying/Saying";
import Motivation from "./Conteiner/Quotes/Componrnt/Motivation/Motivation";
import './App.css';

function App() {
  return (
    <div className="App">
        <div className="top"/>
        <div className="main-block">
            <h2 className="mainTitle">Quotes Central</h2>
            <BrowserRouter>
                <ul className="ul">
                    <li className="liNav"><NavLink to="/" exact activeStyle={{color:'steelblue'}}>Quotes</NavLink></li>
                    <li className="liNav"><NavLink to="/submit" activeStyle={{color:'steelblue'}}>Submit new quote</NavLink></li>
                </ul>
                <div className="switch">
                    <ul>
                        <li><NavLink to="/quote" exact activeStyle={{color:'steelblue'}}>All</NavLink></li>
                        <li><NavLink to="/quote/star" activeStyle={{color:'steelblue'}}>Star Wars</NavLink></li>
                        <li><NavLink to="/quote/people" activeStyle={{color:'steelblue'}}>Famous people</NavLink></li>
                        <li><NavLink to="/quote/humor" activeStyle={{color:'steelblue'}}>Humor</NavLink></li>
                        <li><NavLink to="/quote/saying" activeStyle={{color:'steelblue'}}>Saying</NavLink></li>
                        <li><NavLink to="/quote/motivation" activeStyle={{color:'steelblue'}}>Motivational</NavLink></li>
                    </ul>
                    <Switch>
                        <Route path="/" exact component={Quotes}/>
                        <Route path="/submit" component={SubmitQuote}/>
                        <Route path="/quote" exact component={Quotes}/>
                        <Route path="/quote/star" component={Star}/>
                        <Route path="/quote/edit" component={Edit}/>
                        <Route path="/quote/people" component={People}/>
                        <Route path="/quote/humor" component={Humor}/>
                        <Route path="/quote/saying" component={Saying}/>
                        <Route path="/quote/motivation" component={Motivation}/>
                        <Route render={() => <h1>Not found</h1>}/>
                    </Switch>
                </div>
            </BrowserRouter>
        </div>
    </div>
  );
}

export default App;

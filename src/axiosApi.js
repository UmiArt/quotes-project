import axios from 'axios';

const axiosApi = axios.create ({
    baseURL: 'https://quotes-project-c62b5-default-rtdb.firebaseio.com'
});

export default axiosApi;
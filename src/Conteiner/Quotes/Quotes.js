import React from 'react';
import {useEffect} from 'react';
import {useState} from "react";
import axios from "axios";
import './Quotes.css';

const Quotes = ({history, match}) => {

    console.log(match.params.id)

    const [getQuotes, setGetQuotes] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get(`https://quotes-project-c62b5-default-rtdb.firebaseio.com/quote.json`);
            const rest = response.data
            setGetQuotes(rest);
        }

        fetchData().catch(e => console.error(e));
    },[]);

    const deleteQuote = async (match) => {
      console.log(match)
        await axios.delete(`https://quotes-project-c62b5-default-rtdb.firebaseio.com/quote/${match.params.id}.json`);
        history.replace('/');
    };

    const editQuote = async e => {
        e.preventDefault();
        history.replace('/quote/edit/' + match.params.id);
    };

    return getQuotes && (
        <div className="allQuotes">
            <h2>All</h2>
            {Object.keys(getQuotes).map(newP => (
                <div className="quote" key={newP.toString()}>
                    <p>Text : {getQuotes[newP].quote.text}</p>
                    <p>Author : {getQuotes[newP].quote.author}</p>
                    <button onClick={deleteQuote}>Delete</button>
                    <button onClick={editQuote}>Edit</button>
                </div>
            ))}
        </div>
    );
};

export default Quotes;
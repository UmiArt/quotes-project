import React from 'react';
import {useState, useEffect} from "react";
import axios from "axios";
const Star = ({match}) => {

    const [star, setStar] = useState(null);

    console.log(match.params.id)

    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get(`https://my-project-app-number-1-default-rtdb.firebaseio.com/post/${match.params.id.category}.json`);
            setStar(response.data);
            console.log(response.data)
        }

        fetchData().catch(e => console.error(e));

    },[match.params.id]);

    return (
        <div>
            <h2>Star Wars</h2>
            {star}
        </div>
    );
};

export default Star;
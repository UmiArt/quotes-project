import React from 'react';
import axios from "axios";
import {useState, useEffect} from "react";

const Edit = ({match, history}) => {

    const [editE, setEditE] = useState(null);

    useEffect(() => {

        const fetchData = async () => {
            const response = await axios.get(`https://quotes-project-c62b5-default-rtdb.firebaseio.com/quote/${match.params.id}.json`);
            setEditE(response.data);
            console.log(response)
        }

        fetchData().catch(e => console.error(e));

    },[match.params.id]);

    const onInputChange = () => {

        setEditE(prev => ({
            ...prev
        }))

    };

    const createQuote= async e => {
        e.preventDefault();
        history.replace('/')

    };

    return (
        <div >
            <form className='form' onSubmit={createQuote}>Category
                <input className="Input"
                       type="text"
                       name="category"
                       value={editE}
                       />Author
                <input className="Input"
                       type="text"
                       name="author"
                       value={editE}
                       />
                Text
                <textarea className='textarea'
                          name="text"
                          value={editE}
                         />
                <button>Save</button>
            </form>
        </div>
    )

};

export default Edit;
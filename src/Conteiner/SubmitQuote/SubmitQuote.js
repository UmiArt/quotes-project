import React from 'react';
import {useState} from "react";
import axiosApi from "../../axiosApi";
import './SubmitQuote.css';

const SubmitQuote = ({history}) => {

    const [quote, setQuote] = useState({
        author: '',
        category: '',
        text: '',
    });

    const onInputChange = e => {

        const {name, value} = e.target;

        setQuote(prev => ({
            ...prev,
            [name]: value
        }))
    };

    const createQuote = async e => {
        e.preventDefault();

        try {
            await axiosApi.post('/quote.json', {
                quote
            })
        } finally {
            history.replace('/')
        }
    };


    return (
        <div className="submit">
            <h3>Submit new quote</h3>
            <form className='form' onSubmit={createQuote}>Category
                <input className="Input"
                       type="text"
                       name="category"
                       value={quote.category}
                       onChange={onInputChange}/>Author
                <input className="Input"
                       type="text"
                       name="author"
                       value={quote.author}
                       onChange={onInputChange}/>
                Text
                <textarea className='textarea'
                          name="text"
                          value={quote.text}
                          onChange={onInputChange}/>
                <button>Save</button>
            </form>
        </div>
    );
};

export default SubmitQuote;